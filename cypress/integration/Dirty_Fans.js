describe ('Dirty_Fans', function(){

    it("Model_visible_checkbox_test",function(){
        cy.visit('https://www.dirtyfans.com/login')
        cy.get('#__layout > div > main > div > div > form.form-login > div:nth-child(5) > label > div.form-inp__wrap > input')
        .type('test')
        cy.get('#__layout > div > main > div > div > form.form-login > div:nth-child(6) > label > div.form-inp__wrap > input')
        .type('123456')
        cy.get('.form-login > .btn--purple')
        .click()
        cy.get('#toggleModelIsEnabled')
        .check()
      })
      

      it("Model_visible_checkbox_eva",function(){
        cy.visit('https://www.dirtyfans.com/login')
        cy.get('#__layout > div > main > div > div > form.form-login > div:nth-child(5) > label > div.form-inp__wrap > input')
        .type('eva')
        cy.get('#__layout > div > main > div > div > form.form-login > div:nth-child(6) > label > div.form-inp__wrap > input')
        .type('123456p')
        cy.get('.form-login > .btn--purple')
        .click()
        cy.get('#toggleModelIsEnabled')
        .check()
          
      })
      

    it('Model_chat_video_upload', () => {
        cy.visit('https://www.dirtyfans.com/login')
        cy.get('#__layout > div > main > div > div > form.form-login > div:nth-child(5) > label > div.form-inp__wrap > input')
        .type('eva')
        cy.get('#__layout > div > main > div > div > form.form-login > div:nth-child(6) > label > div.form-inp__wrap > input')
        .type('123456p')
        cy.get('#__layout > div > main > div > div > form.form-login > button')
        .click()
        cy.get('#chat > section > div > div > div.model > div > div > a > img')
        .click()
        cy.contains('fiha')
        .click()
        cy.fixture('Video_mp4.mp4', 'binary')
      .then(Cypress.Blob.binaryStringToBlob)
      .then(fileContent => {
        cy.get('input[type="file"]').attachFile({
          fileContent,
          fileName: 'Video_mp4.mp4',
          mimeType: 'video/mp4',
          encoding: 'utf8'
        })
    })
        cy.get('.panel-nav__btn-send')
        .click()
    })


    it('Model_chat_picture_upload', () => {
        cy.visit('https://www.dirtyfans.com/login')
        cy.get('#__layout > div > main > div > div > form.form-login > div:nth-child(5) > label > div.form-inp__wrap > input')
        .type('eva')
        cy.get('#__layout > div > main > div > div > form.form-login > div:nth-child(6) > label > div.form-inp__wrap > input')
        .type('123456p')
        cy.get('#__layout > div > main > div > div > form.form-login > button')
        .click()
        cy.get('#chat > section > div > div > div.model > div > div > a > img')
        .click()
        cy.contains('fiha')
        .click()
        cy.fixture('testPicture.png').then(fileContent => {
            cy.get('input[type="file"]').attachFile({
                fileContent: fileContent.toString(),
                fileName: 'testPicture.png',
                mimeType: 'image/png'
            })
    
        })
        cy.get('.panel-nav__btn-send')
        .click()
    })

    it('Model_chat_picture_fileName_message_price', () => {
        cy.visit('https://www.dirtyfans.com/login')
        cy.get('#__layout > div > main > div > div > form.form-login > div:nth-child(5) > label > div.form-inp__wrap > input')
        .type('eva')
        cy.get('#__layout > div > main > div > div > form.form-login > div:nth-child(6) > label > div.form-inp__wrap > input')
        .type('123456p')
        cy.get('#__layout > div > main > div > div > form.form-login > button')
        .click()
        cy.get('#chat > section > div > div > div.model > div > div > a > img')
        .click()
        cy.contains('fiha')
        .click()
        cy.fixture('testPicture.png').then(fileContent => {
            cy.get('input[type="file"]').attachFile({
                fileContent: fileContent.toString(),
                fileName: 'testPicture.png',
                mimeType: 'image/png'
            })
    
        })
        cy.get('.media-demo__file-name-field')
        .type('testPicture.png')
        cy.get('.panel-field')
        .type('Test message')
        cy.get('.panel-nav > :nth-child(3) > .icon-default')
        .click()
        cy.get('#price')
        .type('555')
        cy.get('[type="submit"] > .color-new-purple')
        .click()
        cy.get('.panel-nav__btn-send')
        .click()
        cy.wait(5000)
    })
    
    
    it('Fan_chat_buy_picture', () => {
        cy.visit('https://www.dirtyfans.com/login')
        cy.get('#__layout > div > main > div > div > form.form-login > div:nth-child(5) > label > div.form-inp__wrap > input')
        .type('fiha')
        cy.get('#__layout > div > main > div > div > form.form-login > div:nth-child(6) > label > div.form-inp__wrap > input')
        .type('123456p')
        cy.get('#__layout > div > main > div > div > form.form-login > button')
        .click()
        cy.get('[href="/chat"]')
        .click()
        cy.wait(5000)
        cy.contains('Eva')
        .click()
        cy.get('.msg-paid-media__content > .btn')
        .click()
        cy.get('.d-flex > :nth-child(3)')
        .click()
        cy.wait(5000)
      })


    it('Model_chat_picture_upload_vault', () => {
        cy.visit('https://www.dirtyfans.com/login')
        cy.get('#__layout > div > main > div > div > form.form-login > div:nth-child(5) > label > div.form-inp__wrap > input')
        .type('eva')
        cy.get('#__layout > div > main > div > div > form.form-login > div:nth-child(6) > label > div.form-inp__wrap > input')
        .type('123456p')
        cy.get('#__layout > div > main > div > div > form.form-login > button')
        .click()
        cy.get('#chat > section > div > div > div.model > div > div > a > img')
        .click()
        cy.contains('fiha')
        .click()
        cy.get('#chat > section > div > div > div.wrapContainer > div.model-profile > div > div.chat-room.chat-room--mobile > div.chat-room-panel > div.panel-nav > button:nth-child(2) > img')
        .click()
        cy.get('#chat > section > div > div > div.wrapContainer > div.model-profile > div > div.chat-room.chat-room--mobile > div.chat-room-panel > div.media.chat-room-panel__media-store > div > div.media-content.scroll-hide > div:nth-child(1) > div > div.media-item-content__head.p-1 > span.media-item-content__radio.ml-1')
        .click()
        cy.get('#chat > section > div > div > div.wrapContainer > div.model-profile > div > div.chat-room.chat-room--mobile > div.chat-room-panel > div.media.chat-room-panel__media-store > div > div:nth-child(4) > button')
        .click()
        cy.get('.panel-nav__btn-send')
        .click()
    
    })

    it('Model_chat_video_upload_fileName_message_price', () => {
        cy.visit('https://www.dirtyfans.com/login')
        cy.get('#__layout > div > main > div > div > form.form-login > div:nth-child(5) > label > div.form-inp__wrap > input')
        .type('eva')
        cy.get('#__layout > div > main > div > div > form.form-login > div:nth-child(6) > label > div.form-inp__wrap > input')
        .type('123456p')
        cy.get('#__layout > div > main > div > div > form.form-login > button')
        .click()
        cy.get('#chat > section > div > div > div.model > div > div > a > img')
        .click()
        cy.contains('fiha')
        .click()
        cy.fixture('Video_mp4.mp4', 'binary')
      .then(Cypress.Blob.binaryStringToBlob)
      .then(fileContent => {
        cy.get('input[type="file"]').attachFile({
          fileContent,
          fileName: 'Video_mp4.mp4',
          mimeType: 'video/mp4',
          encoding: 'utf8'
        })
    })
        cy.get('.panel-field')
        .type('Test message')
        cy.get('.panel-nav > :nth-child(3) > .icon-default')
        .click()
        cy.get('#price')
        .type('555')
        cy.get('[type="submit"] > .color-new-purple')
        .click()
        cy.get('.panel-nav__btn-send')
        .click()
        cy.wait(15000)
    })
    
    
    it('Fan_chat_buy_video', () => {
        cy.visit('https://www.dirtyfans.com/login')
        cy.get('#__layout > div > main > div > div > form.form-login > div:nth-child(5) > label > div.form-inp__wrap > input')
        .type('fiha')
        cy.get('#__layout > div > main > div > div > form.form-login > div:nth-child(6) > label > div.form-inp__wrap > input')
        .type('123456p')
        cy.get('#__layout > div > main > div > div > form.form-login > button')
        .click()
        cy.get('[href="/chat"]')
        .click()
        cy.wait(1000)
        cy.contains('Eva')
        .click()
        cy.get('.msg-paid-media__content > .btn')
        .click()
        cy.get('.d-flex > :nth-child(3)')
        .click()
        cy.wait(1000)
      })  
      it('Model_chat_video_upload_vault_fileName_message_price', () => {
        cy.visit('https://www.dirtyfans.com/login')
        cy.get('#__layout > div > main > div > div > form.form-login > div:nth-child(5) > label > div.form-inp__wrap > input')
        .type('eva')
        cy.get('#__layout > div > main > div > div > form.form-login > div:nth-child(6) > label > div.form-inp__wrap > input')
        .type('123456p')
        cy.get('#__layout > div > main > div > div > form.form-login > button')
        .click()
        cy.get('#chat > section > div > div > div.model > div > div > a > img')
        .click()
        cy.contains('fiha')
        .click()
        cy.get('#chat > section > div > div > div.wrapContainer > div.model-profile > div > div.chat-room.chat-room--mobile > div.chat-room-panel > div.panel-nav > button:nth-child(2) > img')
        .click()
        cy.get('#chat > section > div > div > div.wrapContainer > div.model-profile > div > div.chat-room.chat-room--mobile > div.chat-room-panel > div.media.chat-room-panel__media-store > div > div.media-content.scroll-hide > div:nth-child(1) > div > div.media-item-content__head.p-1 > span.media-item-content__radio.ml-1')
        .click()
        cy.get('#chat > section > div > div > div.wrapContainer > div.model-profile > div > div.chat-room.chat-room--mobile > div.chat-room-panel > div.media.chat-room-panel__media-store > div > div:nth-child(4) > button')
        .click()
        cy.get('.media-demo__file-name-field')
        .type('Test_video_mp4')
        cy.get('.panel-field')
        .type('Test message')
        cy.get('.panel-nav > :nth-child(3) > .icon-default')
        .click()
        cy.get('#price')
       .type('555')
        cy.get('[type="submit"] > .color-new-purple')
        .click()
        cy.get('.panel-nav__btn-send')
        .click()
        cy.wait(5000)
    
    })
    
    
    it('Fan_chat_buy_video_fromVault', () => {
        cy.visit('https://www.dirtyfans.com/login')
        cy.get('#__layout > div > main > div > div > form.form-login > div:nth-child(5) > label > div.form-inp__wrap > input')
        .type('fiha')
        cy.get('#__layout > div > main > div > div > form.form-login > div:nth-child(6) > label > div.form-inp__wrap > input')
        .type('123456p')
        cy.get('#__layout > div > main > div > div > form.form-login > button')
        .click()
        cy.get('[href="/chat"]')
        .click()
        cy.wait(5000)
        cy.contains('Eva')
        .click()
        cy.get('.msg-paid-media__content > .btn')
        .click()
        cy.get('.d-flex > :nth-child(3)')
        .click()
        cy.wait(5000)
      })
    
    


    it('Fan_chat_video', () => {
        cy.visit('https://www.dirtyfans.com/login')
        cy.get('#__layout > div > main > div > div > form.form-login > div:nth-child(5) > label > div.form-inp__wrap > input')
        .type('fiha')
        cy.get('#__layout > div > main > div > div > form.form-login > div:nth-child(6) > label > div.form-inp__wrap > input')
        .type('123456p')
        cy.get('#__layout > div > main > div > div > form.form-login > button')
        .click()
        cy.get('[href="/chat"]')
        .click()
        cy.wait(5000)
        cy.contains('Eva')
        .click()
        cy.fixture('Video_mp4.mp4', 'binary')
        .then(Cypress.Blob.binaryStringToBlob)
        .then(fileContent => {
          cy.get('input[type="file"]').attachFile({
            fileContent,
            fileName: 'Video_mp4.mp4',
            mimeType: 'video/mp4',
            encoding: 'utf8'
          })
      })
        cy.get('.chat-messages-panel__bars > .btn--purple')
         .click()
      })


      it('Fan_chat_picture_png', () => {
        cy.visit('https://www.dirtyfans.com/login')
        cy.get('#__layout > div > main > div > div > form.form-login > div:nth-child(5) > label > div.form-inp__wrap > input')
        .type('fiha')
        cy.get('#__layout > div > main > div > div > form.form-login > div:nth-child(6) > label > div.form-inp__wrap > input')
        .type('123456p')
        cy.get('#__layout > div > main > div > div > form.form-login > button')
        .click()
        cy.get('[href="/chat"]')
        .click()
        cy.wait(5000)
        cy.contains('Eva')
        .click()
        cy.fixture('testPicture.png').then(fileContent => {
          cy.get('input[type="file"]').attachFile({
              fileContent: fileContent.toString(),
              fileName: 'testPicture.png',
              mimeType: 'image/png'
          });
    
      });
        cy.get('.chat-messages-panel__bars > .btn--purple')
         .click()
      })
    
    
    it("Login_user",function(){
        cy.visit('https://www.dirtyfans.com/login')
        cy.get('#__layout > div > main > div > div > form.form-login > div:nth-child(5) > label > div.form-inp__wrap > input')
        .type('fiha')
        cy.get('#__layout > div > main > div > div > form.form-login > div:nth-child(6) > label > div.form-inp__wrap > input')
        .type('123456p')
        cy.get('#__layout > div > main > div > div > form.form-login > button')
        .click()
        
    })


    it("Login_model",function(){
        cy.visit('https://www.dirtyfans.com/login')
        cy.get('#__layout > div > main > div > div > form.form-login > div:nth-child(5) > label > div.form-inp__wrap > input')
        .type('eva')
        cy.get('#__layout > div > main > div > div > form.form-login > div:nth-child(6) > label > div.form-inp__wrap > input')
        .type('123456p')
        cy.get('#__layout > div > main > div > div > form.form-login > button')
        .click()
        
    })


    it("Login_admin",function(){
        cy.visit('https://www.dirtyfans.com/login')
        cy.get('#__layout > div > main > div > div > form.form-login > div:nth-child(5) > label > div.form-inp__wrap > input')
        .type('user_admin_2')
        cy.get('#__layout > div > main > div > div > form.form-login > div:nth-child(6) > label > div.form-inp__wrap > input')
        .type('i1ovwxJOPTupGH2w')
        cy.get('#__layout > div > main > div > div > form.form-login > button')
        .click()
    })


    it("Login_account_negative_english",function(){
        cy.visit('https://www.dirtyfans.com/login')
        cy.get('#__layout > div > main > div > div > form.form-login > div:nth-child(5) > label > div.form-inp__wrap > input')
        .type('fiha321321321')
        cy.get('#__layout > div > main > div > div > form.form-login > div:nth-child(6) > label > div.form-inp__wrap > input')
        .type('123456p')
        cy.get('#__layout > div > main > div > div > form.form-login > button')
        .click()
        cy.contains('Wrong username')
        
    })
    

    it("Login_password_negative_english",function(){
        cy.visit('https://www.dirtyfans.com/login')
        cy.get('#__layout > div > main > div > div > form.form-login > div:nth-child(5) > label > div.form-inp__wrap > input')
        .type('fiha')
        cy.get('#__layout > div > main > div > div > form.form-login > div:nth-child(6) > label > div.form-inp__wrap > input')
        .type('123456pdasdasdad')
        cy.get('#__layout > div > main > div > div > form.form-login > button')
        .click()
        cy.contains('Wrong password')
        
    })


    it("Login_account_negative_spain",function(){
        cy.visit('https://www.dirtyfans.com/login')
        cy.get('#__layout > div > main > div > div > form.form-login > div:nth-child(5) > label > div.form-inp__wrap > input')
        .type('fiha321321321')
        cy.get('#__layout > div > main > div > div > form.form-login > div:nth-child(6) > label > div.form-inp__wrap > input')
        .type('123456p')
        cy.get('#__layout > div > header > div > div > div.language-switch.main-header__lang.ml-2 > img')
        .click()
        cy.get('#__layout > div > header > div > div > div.language-switch.main-header__lang.ml-2 > div > div:nth-child(2)')
        .click()
        cy.get('#__layout > div > main > div > div > form.form-login > button')
        .click()
        cy.contains('Nombre de usuario erróneo')
        
    })


    it("Login_password_negative_spain",function(){
        cy.visit('https://www.dirtyfans.com/login')
        cy.get('#__layout > div > main > div > div > form.form-login > div:nth-child(5) > label > div.form-inp__wrap > input')
        .type('fiha')
        cy.get('#__layout > div > main > div > div > form.form-login > div:nth-child(6) > label > div.form-inp__wrap > input')
        .type('123456padsasdasd')
        cy.get('#__layout > div > header > div > div > div.language-switch.main-header__lang.ml-2 > img')
        .click()
        cy.get('#__layout > div > header > div > div > div.language-switch.main-header__lang.ml-2 > div > div:nth-child(2)')
        .click()
        cy.get('#__layout > div > main > div > div > form.form-login > button')
        .click()
        cy.contains('Contraseña errónea')
        
    })


    it("Registration_underskore",function(){
        cy.visit('https://www.dirtyfans.com/register')
        cy.get('#__layout > div > main > div > div > form:nth-child(5) > div:nth-child(7) > label > div.form-inp__wrap > input')
        .type('test_under')
        cy.get('#__layout > div > main > div > div > form:nth-child(5) > div:nth-child(8) > label > div.form-inp__wrap > input')
        .type('sfafas@gmail.com')
        cy.get('#__layout > div > main > div > div > form:nth-child(5) > div:nth-child(9) > label > div.form-inp__wrap > input')
        .type('123456p')
        cy.get('#__layout > div > main > div > div > form:nth-child(5) > div:nth-child(10) > div > a:nth-child(1)')
        .click()
        cy.get('#__layout > div > main > div > div > form:nth-child(5) > div.form-register__agree.text-sm.mb-5 > label')
        .click()
        cy.get('#__layout > div > main > div > div > form:nth-child(5) > button')
        .click()
        cy.contains('Username is incorrect')
    
        
        
    })


    it("Start_stream",function(){
        cy.visit('https://www.dirtyfans.com/login')
        cy.get('#__layout > div > main > div > div > form.form-login > div:nth-child(5) > label > div.form-inp__wrap > input')
        .type('eva')
        cy.get('#__layout > div > main > div > div > form.form-login > div:nth-child(6) > label > div.form-inp__wrap > input')
        .type('123456p')
        cy.get('#__layout > div > main > div > div > form.form-login > button')
        .click()
        cy.get('#chat > section > div > div > div.model > div > div > button > svg')
        .click()
        cy.get('[href="https://www.dirtyfans.com/model/publish"] > .model-mobile-navbar__link-text').click()
        cy.get('#chat > div > div > div.wrapContainer > div.app-web-stream__container > div > div > div.stream-publish-video-player > div.stream-publish-video-player-overlay > img')
        .click()
    
    })


    it("Forgot_password_wrong_mail_english",function(){
        cy.visit('https://www.dirtyfans.com/lost-password')
        cy.get('input')
        .type('zvzvzv')
        cy.get('#__layout > div > main > div > form > div.d-flex.justify-content-center > button')
        .click()
        cy.contains('Email is incorrect')
    })


    it("Forgot_password_wrong_mail_espanol",function(){
        cy.visit('https://www.dirtyfans.com/lost-password')
        cy.get('input')
        .type('zvzvzv')
        cy.get('#__layout > div > main > div > form > div.d-flex.justify-content-center > button')
        .click()
        cy.get('#__layout > div > header > div > div > div.language-switch.main-header__lang.ml-2 > img')
        .click()
        cy.get('#__layout > div > header > div > div > div.language-switch.main-header__lang.ml-2 > div > div:nth-child(2)')
        .click()
        cy.contains('Correo electronico incorrecto')
    })


    it("Forgot_password_positive_english",function(){
        cy.visit('https://www.dirtyfans.com/lost-password')
        cy.get('input')
        .type('qa.dpt.td@gmail.com')
        cy.get('#__layout > div > main > div > form > div.d-flex.justify-content-center > button')
        .click()
        cy.contains('We have e-mailed your password reset link!') 
    })
   

    it("Forgot_password_positive_espanol",function(){
        cy.visit('https://www.dirtyfans.com/lost-password')
        cy.get('#__layout > div > header > div > div > div.language-switch.main-header__lang.ml-2 > img')
        .click()
        cy.get('#__layout > div > header > div > div > div.language-switch.main-header__lang.ml-2 > div > div:nth-child(2)')
        .click()
        cy.get('input')
        .type('qa.dpt.td@gmail.com')
        cy.get('#__layout > div > main > div > form > div.d-flex.justify-content-center > button')
        .click()
        cy.contains('¡Hemos enviado un correo electrónico con el enlace de restablecimiento de contraseña!') 
    })

    it("Upload_free_for_fans_photo_jpg_1",function(){
        cy.visit('https://www.dirtyfans.com/login')
        cy.get('#__layout > div > main > div > div > form.form-login > div:nth-child(5) > label > div.form-inp__wrap > input')
        .type('test')
        cy.get('#__layout > div > main > div > div > form.form-login > div:nth-child(6) > label > div.form-inp__wrap > input')
        .type('123456')
        cy.get('#__layout > div > main > div > div > form.form-login > button')
        .click()
        cy.get('#chat > section > div > div > div.model > div > div > button > svg')
        .click()
        cy.get('#js-model-sidebar > div > div.model-mobile-navbar > a:nth-child(1) > div')
        .click();
        cy.get('#attachmentTitle')
        .type('TestAutomation_jpg_1')
        cy.get('#attachmentPrice')
        .type('00000')
        cy.get('#videoFile')
        .attachFile('Photo_jpg.jpg')
        cy.get('#storeModelVideo')
        .click()
        cy.wait(1000)
        
    })
    
    
    it("Upload_free_for_fans_photo_jpeg_2",function(){
        cy.visit('https://www.dirtyfans.com/login')
        cy.get('#__layout > div > main > div > div > form.form-login > div:nth-child(5) > label > div.form-inp__wrap > input')
        .type('test')
        cy.get('#__layout > div > main > div > div > form.form-login > div:nth-child(6) > label > div.form-inp__wrap > input')
        .type('123456')
        cy.get('#__layout > div > main > div > div > form.form-login > button')
        .click()
        cy.get('#chat > section > div > div > div.model > div > div > button > svg')
        .click()
        cy.get('#js-model-sidebar > div > div.model-mobile-navbar > a:nth-child(1) > div')
        .click();
        cy.get('#attachmentTitle')
        .type('TestAutomation_jpeg_2')
        cy.get('#attachmentPrice')
        .type('00000')
        cy.get('#videoFile')
        .attachFile('Photo_jpeg.jpeg')
        cy.get('#storeModelVideo')
        .click()
        cy.wait(1000)
        
    })
    
    
    it("Upload_free_for_fans_photo_png_3",function(){
        cy.visit('https://www.dirtyfans.com/login')
        cy.get('#__layout > div > main > div > div > form.form-login > div:nth-child(5) > label > div.form-inp__wrap > input')
        .type('test')
        cy.get('#__layout > div > main > div > div > form.form-login > div:nth-child(6) > label > div.form-inp__wrap > input')
        .type('123456')
        cy.get('#__layout > div > main > div > div > form.form-login > button')
        .click()
        cy.get('#chat > section > div > div > div.model > div > div > button > svg')
        .click()
        cy.get('#js-model-sidebar > div > div.model-mobile-navbar > a:nth-child(1) > div')
        .click();
        cy.get('#attachmentTitle')
        .type('TestAutomation_png_3')
        cy.get('#attachmentPrice')
        .type('00000')
        cy.get('#videoFile')
        .attachFile('Photo_png.png')
        cy.get('#storeModelVideo')
        .click()
        cy.wait(1000)
        
    })
    
    
    it("Upload_free_for_fans_photo_webp_4",function(){
        cy.visit('https://www.dirtyfans.com/login')
        cy.get('#__layout > div > main > div > div > form.form-login > div:nth-child(5) > label > div.form-inp__wrap > input')
        .type('test')
        cy.get('#__layout > div > main > div > div > form.form-login > div:nth-child(6) > label > div.form-inp__wrap > input')
        .type('123456')
        cy.get('#__layout > div > main > div > div > form.form-login > button')
        .click()
        cy.get('#chat > section > div > div > div.model > div > div > button > svg')
        .click()
        cy.get('#js-model-sidebar > div > div.model-mobile-navbar > a:nth-child(1) > div')
        .click();
        cy.get('#attachmentTitle')
        .type('TestAutomation_webp_4')
        cy.get('#attachmentPrice')
        .type('00000')
        cy.get('#videoFile')
        .attachFile('Photo_webp.webp')
        cy.get('#storeModelVideo')
        .click()
        cy.wait(1000)
        
    })
    
    
    it("Upload_free_for_fans_video_mp4_5",function(){
        cy.visit('https://www.dirtyfans.com/login')
        cy.get('#__layout > div > main > div > div > form.form-login > div:nth-child(5) > label > div.form-inp__wrap > input')
        .type('test')
        cy.get('#__layout > div > main > div > div > form.form-login > div:nth-child(6) > label > div.form-inp__wrap > input')
        .type('123456')
        cy.get('#__layout > div > main > div > div > form.form-login > button')
        .click()
        cy.get('#chat > section > div > div > div.model > div > div > button > svg')
        .click()
        cy.get('#js-model-sidebar > div > div.model-mobile-navbar > a:nth-child(1) > div')
        .click();
        cy.get('#attachmentTitle')
        .type('TestAutomation_mp4_5')
        cy.get('#attachmentPrice')
        .type('00000')
        cy.get('#videoFile')
        .attachFile('Video_mp4.mp4')
        cy.get('#storeModelVideo')
        .click()
        cy.wait(25000)
        
    })
    
    
    it("Upload_free_for_fans_video_avi_6",function(){
        cy.visit('https://www.dirtyfans.com/login')
        cy.get('#__layout > div > main > div > div > form.form-login > div:nth-child(5) > label > div.form-inp__wrap > input')
        .type('test')
        cy.get('#__layout > div > main > div > div > form.form-login > div:nth-child(6) > label > div.form-inp__wrap > input')
        .type('123456')
        cy.get('#__layout > div > main > div > div > form.form-login > button')
        .click()
        cy.get('#chat > section > div > div > div.model > div > div > button > svg')
        .click()
        cy.get('#js-model-sidebar > div > div.model-mobile-navbar > a:nth-child(1) > div')
        .click();
        cy.get('#attachmentTitle')
        .type('TestAutomation_avi_6')
        cy.get('#attachmentPrice')
        .type('00000')
        cy.get('#videoFile')
        .attachFile('Video_avi.avi')
        cy.get('#storeModelVideo')
        .click()
        cy.wait(5000)
        
    })
    
    
    it("Upload_free_for_fans_video_mov_7",function(){
        cy.visit('https://www.dirtyfans.com/login')
        cy.get('#__layout > div > main > div > div > form.form-login > div:nth-child(5) > label > div.form-inp__wrap > input')
        .type('test')
        cy.get('#__layout > div > main > div > div > form.form-login > div:nth-child(6) > label > div.form-inp__wrap > input')
        .type('123456')
        cy.get('#__layout > div > main > div > div > form.form-login > button')
        .click()
        cy.get('#chat > section > div > div > div.model > div > div > button > svg')
        .click()
        cy.get('#js-model-sidebar > div > div.model-mobile-navbar > a:nth-child(1) > div')
        .click();
        cy.get('#attachmentTitle')
        .type('TestAutomation_mov_7')
        cy.get('#attachmentPrice')
        .type('00000')
        cy.get('#videoFile')
        .attachFile('Video_mov.mov')
        cy.get('#storeModelVideo')
        .click()
        cy.wait(10000)
        
    })
    
    
    it("Upload_everybody_pay_photo_jpg_8",function(){
        cy.visit('https://www.dirtyfans.com/login')
        cy.get('#__layout > div > main > div > div > form.form-login > div:nth-child(5) > label > div.form-inp__wrap > input')
        .type('test')
        cy.get('#__layout > div > main > div > div > form.form-login > div:nth-child(6) > label > div.form-inp__wrap > input')
        .type('123456')
        cy.get('#__layout > div > main > div > div > form.form-login > button')
        .click()
        cy.get('#chat > section > div > div > div.model > div > div > button > svg')
        .click()
        cy.get('#js-model-sidebar > div > div.model-mobile-navbar > a:nth-child(1) > div')
        .click();
        cy.get('#attachmentTitle')
        .type('TestAutomation_jpg_8')
        cy.get('#not_visible_for_fans')
        .click()
        cy.get('#attachmentPrice')
        .type('00000')
        cy.get('#videoFile')
        .attachFile('Photo_jpg.jpg')
        cy.get('#storeModelVideo')
        .click()
        cy.wait(500)
        
    })
    
    
    it("Upload_everybody_pay_video_mp4_9",function(){
        cy.visit('https://www.dirtyfans.com/login')
        cy.get('#__layout > div > main > div > div > form.form-login > div:nth-child(5) > label > div.form-inp__wrap > input')
        .type('test')
        cy.get('#__layout > div > main > div > div > form.form-login > div:nth-child(6) > label > div.form-inp__wrap > input')
        .type('123456')
        cy.get('#__layout > div > main > div > div > form.form-login > button')
        .click()
        cy.get('#chat > section > div > div > div.model > div > div > button > svg')
        .click()
        cy.get('#js-model-sidebar > div > div.model-mobile-navbar > a:nth-child(1) > div')
        .click();
        cy.get('#attachmentTitle')
        .type('TestAutomation_mp4_9')
        cy.get('#not_visible_for_fans')
        .click()
        cy.get('#attachmentPrice')
        .type('00000')
        cy.get('#videoFile')
        .attachFile('Video_mp4.mp4')
        cy.get('#storeModelVideo')
        .click()
        cy.wait(25000)
        
    })
    
    
    it("Upload_free_photo_jpg_10",function(){
        cy.visit('https://www.dirtyfans.com/login')
        cy.get('#__layout > div > main > div > div > form.form-login > div:nth-child(5) > label > div.form-inp__wrap > input')
        .type('test')
        cy.get('#__layout > div > main > div > div > form.form-login > div:nth-child(6) > label > div.form-inp__wrap > input')
        .type('123456')
        cy.get('#__layout > div > main > div > div > form.form-login > button')
        .click()
        cy.get('#chat > section > div > div > div.model > div > div > button > svg')
        .click()
        cy.get('#js-model-sidebar > div > div.model-mobile-navbar > a:nth-child(1) > div')
        .click();
        cy.get('#attachmentTitle')
        .type('TestAutomation_jpg_10')
        cy.get('#free')
        .click()
        cy.get('#videoFile')
        .attachFile('Photo_jpg.jpg')
        cy.get('#storeModelVideo')
        .click()
        cy.wait(1000)
        
    })
    
    
    it("Upload_free_video_mp4_11",function(){
        cy.visit('https://www.dirtyfans.com/login')
        cy.get('#__layout > div > main > div > div > form.form-login > div:nth-child(5) > label > div.form-inp__wrap > input')
        .type('test')
        cy.get('#__layout > div > main > div > div > form.form-login > div:nth-child(6) > label > div.form-inp__wrap > input')
        .type('123456')
        cy.get('#__layout > div > main > div > div > form.form-login > button')
        .click()
        cy.get('#chat > section > div > div > div.model > div > div > button > svg')
        .click()
        cy.get('#js-model-sidebar > div > div.model-mobile-navbar > a:nth-child(1) > div')
        .click();
        cy.get('#attachmentTitle')
        .type('TestAutomation_mp4_11')
        cy.get('#free')
        .click()
        cy.get('#videoFile')
        .attachFile('Video_mp4.mp4')
        cy.get('#storeModelVideo')
        .click()
        cy.wait(25000)
        
    })



    it("Model_profile_avatar_background",function(){
        cy.visit('https://www.dirtyfans.com/login')
        cy.get('#__layout > div > main > div > div > form.form-login > div:nth-child(5) > label > div.form-inp__wrap > input')
        .type('eva')
        cy.get('#__layout > div > main > div > div > form.form-login > div:nth-child(6) > label > div.form-inp__wrap > input')
        .type('123456p')
        cy.get('#__layout > div > main > div > div > form.form-login > button')
        .click()
        cy.get('#uploadPhoto > div.bottomLine > div > button.btn--active.btnEdit')
        .click()
        
        cy.get('.item-img')
        .attachFile('Avatar.jpg')
        cy.get('.upload-box-cropper-wrap__btns > :nth-child(3)')
        .click()
        cy.wait(10000)
        cy.get('.btnEdit')
        .click()
        cy.get('.uploadPhotoProfile')
        .attachFile('Background.jpg')
        cy.get('.upload-box-cropper-wrap__btns > :nth-child(3)')
        .click()  
      })


      it("Upload_free_without_tittle",function(){
        cy.visit('https://www.dirtyfans.com/login')
        cy.get('#__layout > div > main > div > div > form.form-login > div:nth-child(5) > label > div.form-inp__wrap > input')
        .type('test')
        cy.get('#__layout > div > main > div > div > form.form-login > div:nth-child(6) > label > div.form-inp__wrap > input')
        .type('123456')
        cy.get('#__layout > div > main > div > div > form.form-login > button')
        .click()
        cy.get('#chat > section > div > div > div.model > div > div > button > svg')
        .click()
        cy.get('#js-model-sidebar > div > div.model-mobile-navbar > a:nth-child(1) > div')
        .click();
        cy.get('#free')
        .click()
        cy.get('#videoFile')
        .attachFile('Video_mp4.mp4')
        cy.get('#storeModelVideo')
        .click()
        cy.contains('Please fill ')
        })


      it("Tips_model profile",function(){
        cy.visit('https://www.dirtyfans.com/eva')
        cy.get('.header-fan-menu-settings > .btn')
        .click()
        cy.get('#__layout > div > main > div > div > form.form-login > div:nth-child(5) > label > div.form-inp__wrap > input')
        .type('fiha')
        cy.get('#__layout > div > main > div > div > form.form-login > div:nth-child(6) > label > div.form-inp__wrap > input')
        .type('123456p')
        cy.get('#__layout > div > main > div > div > form.form-login > button')
        .click()
        cy.get(':nth-child(4) > .btn')
        .click()
        cy.contains('You have sent 100 tokens to model') 
      })    
   

      it("Model_profile_button_click",function(){
        cy.visit('https://www.dirtyfans.com/login')
        cy.get('#__layout > div > main > div > div > form.form-login > div:nth-child(5) > label > div.form-inp__wrap > input')
        .type('fiha')
        cy.get('#__layout > div > main > div > div > form.form-login > div:nth-child(6) > label > div.form-inp__wrap > input')
        .type('123456p')
        cy.get('#__layout > div > main > div > div > form.form-login > button')
        .click()
        cy.get('#fan > main > div > div.content-fan.mb-4 > div:nth-child(1) > div > div.content-join > a')
        .click({force:true})
      })
    

      it("Model_registration",function(){
        cy.visit('https://www.dirtyfans.com/register')
        cy.get(':nth-child(7) > .form-inp > .form-inp__wrap > .form-inp__field')
        .type('testmodelaut')
        cy.get(':nth-child(8) > .form-inp > .form-inp__wrap > .form-inp__field')
        .type('qafsasadssadfasf@gmail.com')
        cy.get(':nth-child(9) > .form-inp > .form-inp__wrap > .form-inp__field')
        .type('123456p')
        cy.get('[href="#"]')
        .click()
        cy.get('.form-register__agree > label')
        .click()
        cy.get('[method="post"] > .btn')
        .click()
        cy.wait(5000)
        cy.get('#first_name')
        .type('Test')
        cy.get('#last_name')
        .type('Test')
        cy.get('#countrySelect')
        .click({force: true})
        .type('Afghanistan{enter}')
        cy.get('.item')
        .click()
        cy.get('#city')
        .type('Testtsdts')
        cy.get('#zip')
        .type('214214114')
        cy.get('#id_legal')
        .type('103842978')
        cy.get('#id_legal_expire')
        .type('12.12.2028')
        cy.get(':nth-child(11) > :nth-child(2) > .upload-box > .upload-box__file')
        .attachFile('Photo_jpg.jpg')
        cy.get('.upload-box-cropper-wrap__btns > :nth-child(3)')
        .click()
        cy.get(':nth-child(13) > :nth-child(2) > .upload-box > .upload-box__file')
        .attachFile('Photo_jpg.jpg')
        cy.get('.upload-box-cropper-wrap__btns > :nth-child(3)')
        .click()
        cy.get('[type="submit"]')
        .click()
        cy.wait(5000)
        cy.clearCookies()
    })
    
    
    
    it("Admin_confirm_new_model",function(){
            cy.visit('https://www.dirtyfans.com/login')
            cy.get('#__layout > div > main > div > div > form.form-login > div:nth-child(5) > label > div.form-inp__wrap > input')
            .type('user_admin_2')
            cy.get('#__layout > div > main > div > div > form.form-login > div:nth-child(6) > label > div.form-inp__wrap > input')
            .type('i1ovwxJOPTupGH2w')
            cy.get('#__layout > div > main > div > div > form.form-login > button')
            .click()
            cy.get('tr.odd:nth-child(1) > td:nth-child(6) > form:nth-child(1) > select:nth-child(1)')
            .select('enabled')
            cy.get('tr.odd:nth-child(1) > td:nth-child(6) > form:nth-child(1) > select:nth-child(1)')
            .parent()
            .parent()
            .parent()
            .contains('testmodelaut')
            cy.get(':nth-child(1) > :nth-child(7) > a > img')
            .click({force: true})
            cy.get('.wrapBtn__confirm > .btn')
            .click()
            cy.wait(5000)
    })
    
    
    it("Model_delete",function(){
        cy.visit('https://www.dirtyfans.com/login')
        cy.get('#__layout > div > main > div > div > form.form-login > div:nth-child(5) > label > div.form-inp__wrap > input')
        .type('user_admin_2')
        cy.get('#__layout > div > main > div > div > form.form-login > div:nth-child(6) > label > div.form-inp__wrap > input')
        .type('i1ovwxJOPTupGH2w')
        cy.get('#__layout > div > main > div > div > form.form-login > button')
        .click()
        cy.get('tr.odd:nth-child(1) > td:nth-child(6) > form:nth-child(1) > select:nth-child(1)')
            .parent()
            .parent()
            .parent()
            .contains('testmodelaut')
        cy.get(':nth-child(1) > :nth-child(9) > .confirmDelete > img')
        .click({force: true})
        cy.get('.confirmationDel')
        .click()  
    })

    it("User_registration",function(){
        cy.visit('https://www.dirtyfans.com/register')
        cy.get(':nth-child(7) > .form-inp > .form-inp__wrap > .form-inp__field')
        .type('testuseraut')
        cy.get(':nth-child(8) > .form-inp > .form-inp__wrap > .form-inp__field')
        .type('tryjitfugewsfa@gmail.com')
        cy.get(':nth-child(9) > .form-inp > .form-inp__wrap > .form-inp__field')
        .type('123456p')
        cy.get('.form-register__agree > label')
        .click()
        cy.get('[method="post"] > .btn')
        .click()
        cy.wait(5000)
        cy.clearCookies() 
    })
    
    it("User_delete",function(){
        cy.visit('https://www.dirtyfans.com/login')
        cy.get('#__layout > div > main > div > div > form.form-login > div:nth-child(5) > label > div.form-inp__wrap > input')
        .type('user_admin_2')
        cy.get('#__layout > div > main > div > div > form.form-login > div:nth-child(6) > label > div.form-inp__wrap > input')
        .type('i1ovwxJOPTupGH2w')
        cy.get('#__layout > div > main > div > div > form.form-login > button')
        .click()
        cy.wait(10000)
        cy.get(':nth-child(2) > .modelLink > p')
        .click({force:true})
        cy.get(':nth-child(1) > :nth-child(10) > .confirmDelete > img')
        .parent()
        .parent()
        .parent()
        .contains('testuseraut')
        cy.get(':nth-child(1) > :nth-child(10) > .confirmDelete > img')
        .click({force: true})
        cy.get('.confirmationDel')
        .click({force: true})
    
    })  

})